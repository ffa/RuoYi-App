/**
* 显示消息提示框
* @param content 提示的标题
*/
export function toast(content) {
  uni.showToast({
    icon: 'none',
    title: content
  })
}

/**
* 显示登录失效模态弹窗
* @param content 提示的标题
*/
export function showConfirm(content) {
  return new Promise((resolve, reject) => {
    uni.showModal({
      title: '提示',
      content: content,
	  showCancel: false,
      confirmText: '确定',
      success: function(res) {
        resolve(res)
      }
    })
  })
}

/**
* 显示工单提示模态弹窗
* @param content 提示的标题
* @param confirmText 确认按钮的文字
* @param cancelText 取消按钮的文字
*/
export function showOrderConfirm(content, confirmText, cancelText) {
  return new Promise((resolve, reject) => {
    uni.showModal({
      title: '提示',
      content: content,
      cancelText: cancelText,
      cancelColor: '#c0c0c0',
      confirmText: confirmText,
      confirmColor: '#0055ff',
      success: function(res) {
        resolve(res)
      }
    })
  })
}

/**
* 参数处理
* @param params 参数
*/
export function tansParams(params) {
  let result = ''
  for (const propName of Object.keys(params)) {
    const value = params[propName]
    var part = encodeURIComponent(propName) + "="
    if (value !== null && value !== "" && typeof (value) !== "undefined") {
      if (typeof value === 'object') {
        for (const key of Object.keys(value)) {
          if (value[key] !== null && value[key] !== "" && typeof (value[key]) !== 'undefined') {
            let params = propName + '[' + key + ']'
            var subPart = encodeURIComponent(params) + "="
            result += subPart + encodeURIComponent(value[key]) + "&"
          }
        }
      } else {
        result += part + encodeURIComponent(value) + "&"
      }
    }
  }
  return result
}

/**
 * @desc 函数防抖 这里用到了闭包 
 *	防抖的分类--防抖分为立即防抖和非立即防抖。
 *	立即防抖：多次触发事件，第一次会立即执行函数，之后在设定wait时间内触发的事件无效，不会执行。
 *	非立即防抖：多次触发事件，只会在最后一次触发事件后等待设定的wait时间结束时执行一次。
 * @param func 目标函数
 * @param wait 延迟执行毫秒数
 * @param immediate true - 立即执行， false - 延迟执行
 */
export const debounce = function(func, wait = 1000, immediate = true) {
	let timer;
	console.log(1);
	return function() {
		console.log(123);
		// 改变执行函数内部this的指向，让this指向container
		let context = this,
			args = arguments; //参数
		// 只清除定时器，不清除timer的值
		if (timer) clearTimeout(timer);
		if (immediate) {
			// 立即执行:多次触发事件，第一次会立即执行函数，之后在设定wait时间内触发的事件无效，不会执行。
			// callNow来记录是否立即执行，callNow为true立即执行
			let callNow = !timer;
			// wait时间内（timer有值，callNow为false)不会立即执行
			// wait时间到了(timer值置为null,callNow为true)立即执行
			timer = setTimeout(() => {
				timer = null;
			}, wait);
			if (callNow) func.apply(context, args);
		} else {
			// 非立即执行：多次触发事件，只会在最后一次触发事件后等待设定的wait时间结束时执行一次。
			timer = setTimeout(() => {
				func.apply(context, args);
			}, wait)
		}
	}
}
/**
 * @desc 函数节流
 * @param func 函数
 * @param wait 延迟执行毫秒数
 * @param type 1 使用表时间戳，在时间段开始的时候触发 2 使用表定时器，在时间段结束的时候触发
 */
export const throttle = (func, wait = 1000, type = 1) => {
	let previous = 0;
	let timeout;
	return function() {
		let context = this;
		let args = arguments;
		if (type === 1) {
			let now = Date.now();

			if (now - previous > wait) {
				func.apply(context, args);
				previous = now;
			}
		} else if (type === 2) {
			if (!timeout) {
				timeout = setTimeout(() => {
					timeout = null;
					func.apply(context, args)
				}, wait)
			}
		}
	}
}
