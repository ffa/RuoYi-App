import store from '../store/index.js'

/**
 * 深拷贝
 * @param {Object} value 
 */
export const deepCopy = (value) => {
	return JSON.parse(JSON.stringify(value))
}

/**
 * 根据key获取result
 * @param {String} key 获取的key，默认'result'
 * @param {Boolean} remove 是否清除掉当前所获取的值，默认清除
 */
export const getResult = (key = 'result', remove = true) => {
	let result = store.state.result[key]
	if (remove) {
		removeResult(key)
	}
	return result
}

/**
 * 根据key保存result
 * @param {String} key 存储的key
 * @param {any} value 存储的value
 */
export const setResult = (key = 'result', value = true) => {
	let obj = {}
	obj[key] = value
	store.dispatch('SetResult', obj)
}

/**
 * 根据key清除对应result
 * @param {String} key 要清除键key
 */
export const removeResult = (key) => {
	store.dispatch('RemoveResult', key)
}

/**
 * 获取页面传递的值
 * @param {Boolean} remove 是否清除
 */
export const getBundle = (remove = true) => {
	let bundle = store.state.bundle
	if (remove && bundle) {
		store.dispatch('SetBundle', '')
	}
	return bundle
}

/**
 * 保存页面传递数据
 * @param {any} value 
 */
export const setBundle = (value) => {
	store.dispatch('SetBundle', value)
}

/**
 * 清除页面传递数据
 */
export const removeBundle = () => {
	store.dispatch('SetBundle', '')
}

export const getLoginInfo = (value = '') => {
	if (value) {
		return store.state.loginInfo[value]
	}
	return store.state.loginInfo
}

export const setStore = (key, value) => {
	return store.dispatch(key, value)
}

export const getClientId = () => {
	let info = plus.push.getClientInfo();
	return info.clientid == 'null' ? '' : info.clientid
}

/**
 * 格式化时间
 * @param {date} date 
 * @param {string} fmt 
 */
export const formatDate = (date = new Date(), fmt = 'yyyy-MM-dd hh:mm:ss') => {
	let o = {
		"M+": date.getMonth() + 1, //月份 
		"d+": date.getDate(), //日 
		"h+": date.getHours(), //小时 
		"m+": date.getMinutes(), //分 
		"s+": date.getSeconds(), //秒 
		"q+": Math.floor(((date.getMonth() + 3) / 3)), //季度 
		"S": date.getMilliseconds() //毫秒 
	};
	if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
	for (let k in o)
		if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[
			k]).substr(("" + o[k]).length)));
	return fmt;
}

/**
 * 复制到剪切板
 * @param {string} 剪切板内容 
 */
export const copyboard = (value) => {
	uni.setClipboardData({
		data: data
	});
}
/**
 * 获取环境变量，baseUrl
 * @param {env} 环境变量 
 */
export const getBaseUrl = (env) => {
	// #ifdef H5
	return ''
	// #endif
	// #ifndef H5
	if(store.state.isDev) {
		return env.dev
	}
	return env.isProd ? env.prod : env.dev
	// #endif
}
/**
 * 曲线配置
 * @param {Array} categories 横轴
 * @param {Array} series 数据轴
 * @param {Number} ymin y轴最小值
 * @param {Number} ymax y轴最大值
 * @param {Number} low 预警线最低值
 * @param {Number} high 预警线最高值
 */
export const getOpts = (categories, series, ymin, ymax, low, high) => {
	let enableMarkLine = isEmpty(low) || isEmpty(high);
	let markLine = {
		type: 'dash',
		dashLength: 5,
		data: [{
			value: high || 0,
			lineColor: '#f04864',
			showLabel: false,
			labelBgOpacity: 0.3
		}, {
			value: low || 0,
			lineColor: '#f04864',
			showLabel: false,
			labelBgOpacity: 0.3
		}]
	}
	let opts = {
		categories: categories,
		series: series,
		xAxis: {
			scrollAlign: 'right'
		},
		yAxis: {
			min: ymin,
			max: ymax
		},
		enableMarkLine: enableMarkLine,
		extra: {
			markLine: enableMarkLine ? markLine : {}
		}
	}
	return deepCopy(opts);
}

/**
 * 判读是否为空
 * @param {any} v 
 * isEmpty()              //true
 * isEmpty([])            //true
 * isEmpty({})            //true
 * isEmpty(0)             //true
 * isEmpty(Number("abc")) //true
 * isEmpty("")            //true
 * isEmpty("   ")         //true
 * isEmpty(false)         //true
 * isEmpty(null)          //true
 * isEmpty(undefined)     //true
 */
export const isEmpty = (v) => {
	switch (typeof v) {
		case 'undefined':
			return true;
		case 'string':
			if (v.replace(/(^[ \t\n\r]*)|([ \t\n\r]*$)/g, '').length == 0) return true;
			break;
		case 'boolean':
			if (!v) return true;
			break;
		case 'number':
			if (0 === v || isNaN(v)) return true;
			break;
		case 'object':
			if (null === v || v.length === 0) return true;
			for (var i in v) {
				return false;
			}
			return true;
	}
	return false;
}

export const setStorage = (key, value, expirse) => {
	let data = {
		value: value,
		expirse: (new Date().getTime() + expirse)
	}
	uni.setStorageSync(key, JSON.stringify(data))
}

export const getStorage = (key) => {
	if(!uni.getStorageSync(key)) {
		return null
	}
	let data = JSON.parse(uni.getStorageSync(key));
	if (data !== null) {
		if (data.expirse != null && data.expirse < new Date().getTime()) {
			uni.removeStorageSync(key);
		} else {
			return data.value;
		}
	}
	return null;
}
// 防止处理多次点击
export const noMultipleClicks = (methods, info) => {
	     // methods是需要点击后需要执行的函数， info是点击需要传的参数
	     let that = this;
	     if (that.noClick) {
		         // 第一次点击
		         that.noClick= false;
		         if(info && info !== '') {
			             // info是执行函数需要传的参数
			             methods(info);
			         } else {
			             methods();
			        }
		         setTimeout(()=> {
			            that.noClick= true;
			         }, 2000)
		    } else {
		        // 这里是重复点击的判断
		     }
	 }
