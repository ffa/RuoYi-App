import Vue from 'vue'
import App from './App'
import store from './store' // store
import plugins from './plugins' // plugins
// import { hasRole, hasPerimission } from '@/utils/auth.js'
import { checkPermi, checkRole } from '@/utils/permission.js' // permission
import check from '@/utils/rules.js' // rules
import { showOrderConfirm, debounce, throttle } from '@/utils/common'

Vue.use(plugins)

Vue.config.productionTip = false
Vue.prototype.$store = store
Vue.prototype.checkPermi = checkPermi
Vue.prototype.checkRole = checkRole
Vue.prototype.$check = check
Vue.prototype.showOrderConfirm = showOrderConfirm
Vue.prototype.debounce = debounce
Vue.prototype.throttle = throttle
App.mpType = 'app'

const app = new Vue({
  ...App
})

app.$mount()
