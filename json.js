const tabList = [{
	name: '全部工单',
	id: '1',
},{
	name: '待抢单',
	id: '2',
},{
	name: '进行中',
	id: '3'
}, {
	name: '已完成',
	id: '4'
}];
const newsList = [{
		id: 1,
		title: '从亲密无间到相爱相杀，这就是人类一败涂地的真相',
		author: 'TapTap',
		images: [
		],
		time: '2小时前',
		type: 1,
	},

	{
		id: 2,
		title: '别再玩手机了，赶紧学前端，晚一年能少掉5根头发',
		author: '爱考过',
		images: [
		],
		time: '30分钟前',
		type: 2,
	},
	{
		id: 3,
		title: '将府公园成居民身边“大绿肺”',
		author: '新京报',
		images: [
		],
		time: '2小时前',
		type: 3,
	},
	{
		id: 4,
		title: '骨傲天最偏爱的五位部下 这么多强者还比不过一只仓鼠',
		author: '神说要唱歌',
		images: [
		],
		time: '2019-04-10 11:43',
		type: 1,
	},
	{
		id: 5,
		title: '继国通倒下后，又一公司放弃快递业务，曾砸20亿战“三通一达”',
		author: '全球加盟网',
		images: [],
		time: '5分钟前',
		type: 2,
	},
	{
		id: 6,
		title: '奔驰车主哭诉维权续：双方再次协商无果',
		author: '环球网',
		images: [],
		time: '5分钟前',
		type: 3,
	},
	{
		id: 7,
		title: '靠跑车激发潜能，奔驰Pro跑车首测，怎么那么像意大利跑车设计',
		author: '车品',
		images: [
		],
		time: '2019-04-14 ：10:58',
		type: 3,
	},
	{
		id: 8,
		title: '程序员浪漫起来有多可怕，看完这3段代码眼睛湿润了!',
		author: '车品',
		images: [
		],
		time: '2019-04-14 ：10:58',
		type: 3,
	},
	{
		id: 9,
		title: '程序员浪漫起来有多可怕，看完这3段代码眼睛湿润了!',
		author: '车品',
		images: [
		],
		time: '2019-04-14 ：10:58',
		type: 3,
	},
	{
		id: 10,
		title: '程序员浪漫起来有多可怕，看完这3段代码眼睛湿润了!',
		author: '车品',
		images: [
		],
		time: '2019-04-14 ：10:58',
		type: 3,
	},
	{
		id: 11,
		title: '程序员浪漫起来有多可怕，看完这3段代码眼睛湿润了!',
		author: '车品',
		images: [
		],
		time: '2019-04-14 ：10:58',
		type: 3,
	},
	{
		id: 12,
		title: '程序员浪漫起来有多可怕，看完这3段代码眼睛湿润了!',
		author: '车品',
		images: [
		],
		time: '2019-04-14 ：10:58',
		type: 3,
	},
]
const evaList = [{
		src: 'http://gss0.baidu.com/-fo3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/77c6a7efce1b9d1663174705fbdeb48f8d546486.jpg',
		nickname: 'Ranth Allngal',
		time: '09-20 12:54',
		zan: '54',
		content: '评论不要太苛刻，不管什么产品都会有瑕疵，客服也说了可以退货并且商家承担运费，我觉得至少态度就可以给五星。'
	},
	{
		src: 'http://img0.imgtn.bdimg.com/it/u=2396068252,4277062836&fm=26&gp=0.jpg',
		nickname: 'Ranth Allngal',
		time: '09-20 12:54',
		zan: '54',
		content: '楼上说的好有道理。'
	}
]

export default {
	tabList,
	newsList,
	evaList
}
