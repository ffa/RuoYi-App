import upload from '@/utils/upload'
import request from '@/utils/request'

// 用户密码重置
export function addWork(title, content) {
  const data = {
    title,
    content
  }
  return request({
    url: '/system/business/profile/updatePwd',
    method: 'post',
    params: data
  })
}
