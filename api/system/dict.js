import request from '@/utils/request'

// 根据字典类型查询字典
export function getDictByType(dictType) {
  return request({
    url: '/system/dict/data/type/' + dictType,
    method: 'get'
  })
}


// 根据字典类型查询字典
export function getDictByTypeNoAuth(dictType) {
  return request({
    url: '/dict/type/' + dictType,
    method: 'get'
  })
}
