import request from '@/utils/request'

// 查询工单管理列表(用户权限)
export function listUserOrder(query) {
    return request({
        url: '/business/order/listByUserAuth',
        method: 'get',
        params: query
    })
}

// 查询工单管理列表(运维权限)
export function listOptOrder(query) {
    return request({
        url: '/business/order/listByOptAuth',
        method: 'get',
        params: query
    })
}

// 查询工单管理列表(管理员权限)
export function listAdminOrder(query) {
    return request({
        url: '/business/order/listByAdmin',
        method: 'get',
        params: query
    })
}

// 查询工单管理详细
export function getOrder(id) {
    return request({
        url: '/business/order/getOrderInfo/' + id,
        method: 'get'
    })
}

// 新增工单管理
export function addOrder(data) {
	// console.log(data);
    return request({
        url: '/business/order/addByApp',
        method: 'post',
        data: data
    })
}

// 修改工单管理
export function updateOrder(data) {
    return request({
        url: '/business/order',
        method: 'put',
        data: data
    })
}

// 删除工单管理
export function delOrder(id) {
    return request({
        url: '/business/order/' + id,
        method: 'delete'
    })
}

// 查询工单数量
export function getOrderNum() {
    return request({
        url: '/business/order/getOrderNum',
        method: 'get'
    })
}

// 接单
export function receiveOrder(data) {
	// console.log(data);
    return request({
        url: '/business/order/receiveOrder',
        method: 'post',
        data: data
    })
}

// 取消工单
export function cancelOrder(data) {
	// console.log(data);
    return request({
        url: '/business/order/cancelOrder',
        method: 'post',
        data: data
    })
}


// 撤销工单
export function revokeOrder(data) {
	// console.log(data);
    return request({
        url: '/business/order/revokeOrder',
        method: 'post',
        data: data
    })
}

// 完成工单
export function finishOrder(data) {
	// console.log(data);
    return request({
        url: '/business/order/finishOrder',
        method: 'post',
        data: data
    })
}

// 评价工单
export function evaluateOrder(data) {
	// console.log(data);
    return request({
        url: '/business/order/evaluateOrder',
        method: 'post',
        data: data
    })
}