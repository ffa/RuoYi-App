import request from '@/utils/request'

// 注册方法
export function register(data) {
  return request({
    'url': '/register',
    headers: {
      isToken: false
    },
    'method': 'post',
    'data': data
  })
}


// 获取openid
export function getOpenid(jsCode) {
  return request({
	url: '/getOpenid?jsCode=' + jsCode,
	method: 'get'
  })
}